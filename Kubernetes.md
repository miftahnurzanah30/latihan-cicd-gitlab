# Kubernetes

aplication for automation deployment, scaling and manajemen aplication  base on container.

### Preparation

| Plugin | Sites |
| ------ | ------ |
| docker | https://docker.com/ |
| kubectl | https://kubernetes.io/docs/tasks/tools/install-kubectl/ |
| minikube | https://kubernetes.io/docs/tasks/tools/install-minikube/ |
| kubernetes on docker | https://medium.com/@damiannolan/kubernetes-on-docker-for-windows-5ca0c6395668|

### Development

1. build image from your project with docker
2. to deploy a service we need to create configuration file using format yaml, example :

```sh
apiVersion: apps/v1
kind: Deployment 
metadata:
  name: nginx
spec:
  replicas: 4 // for scalling the pods
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      name: nginx
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx // name image of your project
          ports:
            - containerPort: 80
```

then execute with kubectl :

```sh
$ kubectl create -f [fileconfig.yaml] 
$ kubectl get deployments //to see list of deployment
```

### Syntax basic kubectl
```sh
$ kubectl get pods //to see list of pod
$ kubectl get rs //to see list of replica set
$ kubectl get deployments // to see list of service running on deployment
```

### more documentation 
|link documents|
|--------------|
|https://docs.google.com/presentation/d/1NJQqJd89k1od_o9Kz-79IQ_CQYDCJKWunCtpgkHQLi4/edit#slide=id.g742fac3576_0_215|
